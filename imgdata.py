import concurrent.futures
import threading
import time
import os
import subprocess
from datetime import datetime as dt

import logs


logger = logs.get(__name__)


class imgfile:
    def __init__(self, direntry, config):
        self._direntry = direntry
        self.name = direntry.name
        self.path = direntry.path
        self._config = config
        self._thumbfile = f"{self._config['datamgr.thumbdir']}/{direntry.name}"

    def get_age(self):
        '''return age in seconds (computed by stat/mtime)'''
        return time.time() - self.get_date()

    def get_date(self):
        return self._direntry.stat().st_mtime

    def manage_thumb(self):
        try:
            stat_thumb = os.stat(self._thumbfile)
            #logger.debug('thumbnail exists')
        except FileNotFoundError:
            #logger.debug('create thumbnail')
            self._config['poolexec'].submit(self.create_thumbnail)

    def create_thumbnail(self):
        logger.debug(f'create thumbnail {self._direntry.name}')
        args = [
                self._config['datamgr.convert_cmd'],
                '-resize', self._config['datamgr.thumbsize'],
                self._direntry.path,
                self._thumbfile,
                ]
        p = subprocess.run(args,
                cwd=self._config['datamgr.imagedir'])
        if p.returncode != 0:
            logger.warning(f'Error creating thumbnail {self._direntry.name}')
            try:
                os.remove(self._thumbfile)
            except FileNotFoundError:
                pass

    def delete_data(self):
        for FN in (self.path, self._thumbfile):
            try:
                logger.debug(f'deleting {FN}')
                stat_imagefile = os.stat(FN)
                os.remove(FN)
            except FileNotFoundError:
                logger.error(f'FileNotFoundError: {FN}')


class Manager():
    def __init__(self, config):
        logger.debug(str(config))
        self._config = config
        self._files_db = dict()
        self._files_list = list()

        self._config['poolexec'] = concurrent.futures.ThreadPoolExecutor(
                max_workers=int(self._config['datamgr.poolworkers']),
                    )
        self.start_monitor()

    def start_monitor(self):
        logger.debug('starting monitor thread')
        self.th = threading.Thread(
                target=self.mainloop
                )
        self.th.start()

    def mainloop(self):
        while True:
            self.scan_dir()
            self.tidyup()
            # after scan and tidy, re-create the index
            self._files_list = list(self._files_db.keys())
            self._files_list.sort()
            time.sleep(int(self._config['datamgr.monitor_secs']))

    def tidyup(self):
        if self._config['datamgr.cleanup.enabled'] != 'True':
            # stop when disabled
            return
        # find old entries and delete
        toberemoved_from_db = list()
        max_age_s = float(self._config['datamgr.cleanup.file_max_age_h']) * 60 * 60
        for K, F in self._files_db.items():
            if F.get_age() > max_age_s:
                F.delete_data()
                toberemoved_from_db.append(K)
        for K in toberemoved_from_db:
            del(self._files_db[K])

    def scan_dir(self):
        def is_eligible(entry):
            return ( entry.is_file() and
                    entry.name.startswith("202") and
                    entry.name.endswith(".jpg") )

        # we are in imgdir, so scan .
        for entry in os.scandir(self._config['datamgr.imagedir']):
            if is_eligible(entry):
                self.add_to_files_db(entry)
            #logger.debug(entry)


    def add_to_files_db(self, entry):
        # create and add imgfile instance to db if needed
        if not entry.name in self._files_db:
            self._files_db[entry.name] = imgfile(entry,
                    self._config)

        we = self._files_db[entry.name]
        we.manage_thumb()

    def get_image_by_index(self, key):
        return self._files_db[key]

    def get_latest_indexes(self, cnt):
        return self._files_list[(-cnt):]

    def get_total_cnt(self):
        return len(self._files_list)

    def get_daterange_indexes(self, datequery):
        '''parse dates'''
        try:
            dstart, dend = datequery.split('..')
        except ValueError:
            dstart = datequery
            dend = ''
        dfmt = "%Y-%m-%dT%H:%M:%S"
        try:
            ddstart = dt.strptime(dstart, dfmt)
        except ValueError:
            ddstart = dt.today()
        try:
            ddend = dt.strptime(dend, dfmt)
        except ValueError:
            ddend = dt.today()

        ldate = ddstart.timestamp()
        rdate = ddend.timestamp()
        logger.debug(f'{ldate}..{rdate}')
        indexes = [ key for key, value in self._files_db.items()
                if value.get_date() >= ldate and value.get_date() <= rdate ]
        indexes.sort()
        return indexes



    def get_agerange_indexes(self, agequery):
        '''convert ages into dates (time) and filter by date'''
        now = time.time()
        try:
            hend, hstart = agequery.split('..')
        except ValueError:
            hstart = agequery
            hend = ''
        try:
            sstart = float(hstart) * 3600
        except ValueError:
            sstart = 7 * 24 * 3600
        try:
            send = float(hend) * 3600
        except ValueError:
            send = 0

        lage = now - sstart
        rage = now - send
        logger.debug(f'{lage}..{rage}')
        indexes = [ key for key, value in self._files_db.items()
                if value.get_date() >= lage and value.get_date() <= rage ]
        indexes.sort()
        return indexes
