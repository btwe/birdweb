import app
import argparse

def parsearg():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", "-c", 
            type=str, default="config.ini")

    return parser.parse_args()


if __name__ == "__main__":
    pa = parsearg()
    app.init(pa.config)
    app.start()
