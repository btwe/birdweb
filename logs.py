"""all logging goes here

configure the root logger
each module in this package should get its own logger
"""
import logging
import sys

logformat = '%(asctime)s %(levelname)s %(name)s "%(message)s"'
logging.basicConfig(
        format=logformat,
        level=logging.NOTSET,
        stream=sys.stderr,
        )

logger = logging.getLogger(__name__)
logger.debug('logging initialized')

def get(name):
    logger.debug(f'Creating logger with name: {name}')
    return logger.getChild(name)

def set_level(level: 'loglevel' = logging.INFO):
    logging.getLogger().setLevel(level)
    logger.debug('logging level set to %s', logging.getLevelName(level))
