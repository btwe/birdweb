import os
import sys

from bottle import Bottle, ConfigDict, run, request, abort, static_file
from bottle import jinja2_view as view, jinja2_template as template

import logs
import imgdata

# config:
TMPL_PATH = ['./tmpl/']
# get a logger
logger = logs.get(__name__)
# the app:
app = Bottle()


@app.route('/', method='GET')
@view('latest.html.j2', template_lookup=TMPL_PATH)
def latest():
    # get latest files according to cnt
    try:
        cnt = int(request.query['cnt'])
    except KeyError:
        # set default
        cnt = int(app.config['main.latest_cnt_images_default'])
    try:
        reverse = request.query['reverse']
        reverse = True
    except KeyError:
        reverse = False
    try:
        age = request.query['age']
    except KeyError:
        age = None
    try:
        date = request.query['date']
    except KeyError:
        date = None

    if age:
        logger.debug(f'Retrieving by age {age}')
        title = f'Images by age {age}'
        description = f'Showing images filtered by agequery, parameters: age={age} (hours), reverse={reverse}'
        indexes = app.config['datamgr'].get_agerange_indexes(age)
    elif date:
        logger.debug(f'Retrieving by date {date}')
        title = f'Images by date {date}'
        description = f'Showing images filtered by datequert, parameters: date={date}, reverse={reverse}'
        indexes = app.config['datamgr'].get_daterange_indexes(date)
    else:
        logger.debug(f'Retrieving latest {cnt}')
        title = f'Latest images {cnt}'
        description = f'Showing the latest images, parameters: cnt={cnt}, reverse={reverse}'
        indexes = app.config['datamgr'].get_latest_indexes(cnt=cnt)

    # indexes queries above, fetch migfiles by indexes
    if reverse: indexes.reverse()
    imgfiles = [ app.config['datamgr'].get_image_by_index(key) for key in indexes ]
    return {
            'title': title,
            'description': description,
            'imgfiles': imgfiles,
            'total': app.config['datamgr'].get_total_cnt(),
            }

@app.route('/img/<imgname:re:.*\.jpg>', method='GET')
def img(imgname):
    try:
        imgsize = request.query['size']
    except KeyError:
        abort(406, "query parameter size is missing. Can be f or t (full img, thumbnail img)")

    if request.query['size'] == 'f':
        basepath = app.config['datamgr.imagedir']
    elif request.query['size'] == 't':
        basepath = app.config['datamgr.thumbdir']
    else:
        abort(406, "query parameter size can either f or t")

    return static_file(imgname, root=basepath)

def init(config_file='config.ini'):
    # load the bottle app config
    app.config.load_config(config_file)
    # create datamgr instance and add it to the config
    app.config['datamgr'] = imgdata.Manager(app.config)
    try:
        # try to mount app itself under the baseurl
        app.mount(app.config['main.baseurl'], app)
        logger.debug(f'using baseurl {app.config["main.baseurl"]}')
    except KeyError:
        pass

def str2bool(s):
    comp = s.lower()
    if  comp == "yes" or \
            comp == "true" or \
            comp == "on" or \
            comp == "1":
        return True
    else:
        return False


def start():
    logger.info('Starting bottle app with internal webserver')
    run(
            app=app,
            host=app.config['main.listen_ip'],
            port=app.config['main.listen_port'],
            reloader=str2bool(app.config['main.reloader']),
            quiet=str2bool(app.config['main.quiet']),
            debug=str2bool(app.config['main.debug']),
            )

