# birdweb

## Description

This is a simple Web Server to view images.

My setup is:

- A bird feed station
- A RPi3
  - with a camera
  - running motion
- motion sends images to my vserver

This application (birdweb) is runnning on the vserver and does the following:

1. web server for image files found in step 2
2. monitoring the in coming img files
    - if new filesare found
      - create an thumbnail

## Installation
```
% pip install bottle jinja2
```

## Configuration
Refer to the sample `config.ini` file.


## Run
bottle is able to run a standalone (for small loads) server. Start with:
```
% python main.py
```

This can be used behind a reverse-proxy (e.g. by nginx or traefik).

bottle also provides a wsgi interface. This is currently not implemented, but
is very little to do.

## License
MIT
